package com.arnoldsiregar.spring.opencsv;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenCsvApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(OpenCsvApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		ReaderExample readerExample = new ReaderExample();

        System.out.println("\nMapped by column name:");
        readerExample.namedColumnExample();

        System.out.println("\nMapped by column position:");
        readerExample.positionColumnExample();
	}
}
