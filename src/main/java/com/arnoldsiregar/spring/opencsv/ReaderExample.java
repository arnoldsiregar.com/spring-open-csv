package com.arnoldsiregar.spring.opencsv;

import com.arnoldsiregar.spring.opencsv.model.UserByColumnName;
import com.arnoldsiregar.spring.opencsv.model.UserByPosition;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class ReaderExample {

    private Reader getReader() throws IOException, URISyntaxException {
        Path path = Paths.get(ClassLoader.getSystemResource("csv/complex.csv").toURI());
        BufferedReader reader = Files.newBufferedReader(Paths.get(path.toUri()));
        return reader;
    }

    public void namedColumnExample() throws IOException, URISyntaxException {
        CsvToBean csvToBean = new CsvToBeanBuilder(getReader())
                .withType(UserByColumnName.class)
                .build();

//        Iterator<UserByColumnName> csvUserIterator = csvToBean.iterator();
//
//        while (csvUserIterator.hasNext()) {
//            UserByColumnName csvUser = csvUserIterator.next();
//            System.out.println("id : " + csvUser.getId());
//            System.out.println("name : " + csvUser.getName());
//            System.out.println("address : " + csvUser.getAddress());
//            System.out.println("==========================");
//        }

        List<UserByColumnName> csvUsers = csvToBean.parse();

        for(UserByColumnName csvUser: csvUsers) {
            System.out.println("id : " + csvUser.getId());
            System.out.println("name : " + csvUser.getName());
            System.out.println("address : " + csvUser.getAddress());
            System.out.println("==========================");
        }
    }

    public void positionColumnExample() throws IOException, URISyntaxException {
        CsvToBean csvToBean = new CsvToBeanBuilder(getReader())
                .withType(UserByPosition.class)
                .withSkipLines(1)   // to skip header row
                .build();

//        Iterator<UserByPosition> csvUserIterator = csvToBean.iterator();
//
//        while (csvUserIterator.hasNext()) {
//            UserByPosition csvUser = csvUserIterator.next();
//            System.out.println("id : " + csvUser.getId());
//            System.out.println("name : " + csvUser.getName());
//            System.out.println("address : " + csvUser.getAddress());
//            System.out.println("==========================");
//        }

        List<UserByPosition> csvUsers = csvToBean.parse();

        for(UserByPosition csvUser: csvUsers) {
            System.out.println("id : " + csvUser.getId());
            System.out.println("name : " + csvUser.getName());
            System.out.println("address : " + csvUser.getAddress());
            System.out.println("==========================");
        }
    }
}
