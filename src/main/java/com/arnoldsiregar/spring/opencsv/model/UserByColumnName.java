package com.arnoldsiregar.spring.opencsv.model;

import com.opencsv.bean.CsvBindByName;

public class UserByColumnName {
    @CsvBindByName(column = "id")
    private Integer id;

    @CsvBindByName
    private String name;

    @CsvBindByName
    private String address;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "UserByColumnName{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }

    public UserByColumnName() {
    }

    public UserByColumnName(Integer id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }
}
